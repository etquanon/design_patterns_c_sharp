﻿class AgentNormal
{
  // address
  public string StreetAddress, Postcode, City;

  // employment
  public string Name, Position, Type;

  public int AnnualIncome;

  public AgentNormal()
  {

  }

  public AgentNormal(string type)
  {
    this.Type = type;
  }

  public override string ToString()
  {
    return $"{nameof(Name)}: {Name}, {nameof(Position)}: {Position}, {nameof(Type)}: {Type}, {nameof(AnnualIncome)}: {AnnualIncome}, {nameof(StreetAddress)}: {StreetAddress}, {nameof(Postcode)}: {Postcode}, {nameof(City)}: {City}";
  }

  public static AgentNormal NewNormalAgent()
  {
    return new AgentNormal();
  }

  public static AgentNormal NewSuperAgent()
  {
    return new AgentNormal("Super");
  }

  public enum AgentType
  {
    AGENT,
    SUPER_AGENT
  }

  // make it lazy
  public static class Factory
  {
    public static AgentNormal NewNormalAgent()
    {
      return new AgentNormal();
    }

    public static AgentNormal NewSuperAgent(string type)
    {
      return new AgentNormal(type);
    }
  }
}
