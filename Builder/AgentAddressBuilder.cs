﻿
public class AgentAddressBuilder : AgentBuilder
{
    // might not work with a value type!
    public AgentAddressBuilder(Agent agent) => this.agent = agent;

    public AgentAddressBuilder At(string streetAddress)
    {
        agent.StreetAddress = streetAddress;
        return this;
    }

    public AgentAddressBuilder WithPostcode(string postcode)
    {
        agent.Postcode = postcode;
        return this;
    }

    public AgentAddressBuilder In(string city)
    {
        agent.City = city;
        return this;
    }

}
