public class AgentJobBuilder : AgentBuilder
{
    public AgentJobBuilder(Agent agent)
    {
        this.agent = agent;
    }

    public AgentJobBuilder Name(string name)
    {
        agent.Name = name;
        return this;
    }

    public AgentJobBuilder AsA(string position)
    {
        agent.Position = position;
        return this;
    }

    public AgentJobBuilder Earning(int annualIncome)
    {
        agent.AnnualIncome = annualIncome;
        return this;
    }
}
