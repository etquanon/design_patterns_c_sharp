﻿public class AgentBuilder
// facade 
{
    // the object we're going to build
    protected Agent agent = new Agent(); // this is a reference!

    public AgentAddressBuilder Lives => new AgentAddressBuilder(agent);
    public AgentJobBuilder Person => new AgentJobBuilder(agent);

    // public static implicit operator Agent(AgentBuilder ag) => ag.agent;
    public Agent build() {
        return agent;
    }
}
