﻿using System;
using BoredLegacy_C_.Bridge;

namespace BoredLegacy_C_
{
    class Program
  {
    static void Main(string[] args)
    {
      bridgeExample();
    }

    static void bridgeExample() {
      var mongo = new EuropeanRenderer();
      var postgre = new OrientalRenderer();

      var collectIntel = new CollectIntel(mongo, 5);
      collectIntel.PlanMission();
      collectIntel.Reparametrize(2.33f);
      collectIntel.PlanMission();

      
    }

    static void builderExample() {
      var agentOne = new AgentBuilder()
          .Person
              .Name("Fabrikam")
              .AsA("Engineer")
              .Earning(123000)
          .Lives
              .At("123 London Road")
              .In("London")
              .WithPostcode("SW12BC");
        
       Console.WriteLine(agentOne.build());
    }

    static void factoryExample() {
      var normalAgent = AgentNormal.Factory.NewSuperAgent("CO998");
      normalAgent.Name = "Vasile";
      normalAgent.City = "Bucuresti";
      normalAgent.Postcode = "010991";
      normalAgent.StreetAddress = "Calea Victoriei 22";
      normalAgent.Position = "Software Engineer";
      normalAgent.AnnualIncome = 130000;

      Console.WriteLine(normalAgent.ToString());
    }
  }
}
