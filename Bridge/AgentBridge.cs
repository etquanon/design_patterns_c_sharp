using static System.Console;

namespace BoredLegacy_C_.Bridge
{
    public interface IRenderer
    {
        void RenderSupply(float supplyCoefficient);
    }

    public class EuropeanRenderer : IRenderer
    {
        public void RenderSupply(float supplyCoefficient)
        {
            WriteLine($"Rendering supply for european mission with {supplyCoefficient}");
        }
    }

    public class OrientalRenderer : IRenderer
    {
        public void RenderSupply(float supplyCoefficient)
        {
            WriteLine($"Rendering supply for middle East mission with {supplyCoefficient}");
        }
    }

    public class LatinoRenderer : IRenderer
    {
        public void RenderSupply(float supplyCoefficient)
        {
            WriteLine($"Rendering supply for middle South America mission with {supplyCoefficient}");
        }
    }

    public abstract class MissionFile
    {
        protected IRenderer renderer;

        // a bridge between the shape that's being drawn an
        // the component which actually draws it
        public MissionFile(IRenderer renderer)
        {
            this.renderer = renderer;
        }

        public abstract void PlanMission();
        public abstract void Reparametrize(float factor);
    }

    public class CollectIntel : MissionFile
    {
        private float intel;

        public CollectIntel(IRenderer renderer, float intel) : base(renderer)
        {
            this.intel = intel;
        }

        public override void PlanMission()
        {
            renderer.RenderSupply(intel);
        }

        public override void Reparametrize(float factor)
        {
            intel *= factor;
        }
    }
}